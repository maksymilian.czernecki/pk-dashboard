package pl.edu.pk.mech;


import eu.hansolo.medusa.*;
import eu.hansolo.medusa.Gauge.SkinType;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;

import java.util.Random;


/**
 * Created by Maksymilian Czernecki on 27.05.2018.
 */
public class Dashboard extends Application {
    private static final Random RND = new Random();
    private static final int DIFF = 60;
    private static final int MIN_THRESHOLD = 30;
    private static final int MAX_THRESHOLD = 150;
    private static final int INTERVAL = 2000;

    private Gauge tempGauge;
    private Gauge batteryGauge;
    private Gauge speedGauge;

    private long lastTimerCall;
    private AnimationTimer timer;

    @Override
    public void init() {
        tempGauge = GaugeBuilder.create()
                .skinType(Gauge.SkinType.QUARTER)
                .animated(true)
                .foregroundBaseColor(Color.WHITE)
                .minValue(0)
                .maxValue(100)
                .title("Battery temperature\n" +
                        "Celsius")
                .sectionsVisible(true)
                .decimals(0)
                .minorTickMarksVisible(false)
                .mediumTickMarksVisible(false)
                .animationDuration(INTERVAL)
                .sections(SectionBuilder.create()
                        .start(80)
                        .stop(100)
                        .color(Color.RED)
                        .build()
                )
                .build();

        batteryGauge = GaugeBuilder.create()
                .skinType(Gauge.SkinType.LEVEL)
                .title("Capacity")
                .titleColor(Color.WHITE)
                .subTitle("%")
                .subTitleColor(Color.WHITE)
                .animated(true)
                .animationDuration(INTERVAL)
                .gradientBarEnabled(true)
                .gradientBarStops(new Stop(0.0, Color.RED),
                        new Stop(0.25, Color.ORANGE),
                        new Stop(0.5, Color.YELLOW),
                        new Stop(0.75, Color.YELLOWGREEN),
                        new Stop(1.0, Color.LIME))
                .build();

        speedGauge = GaugeBuilder.create()
                .skinType(SkinType.GAUGE)
                .decimals(0)
                .minValue(0)
                .maxValue(300)
                .title("PK Mech Power")
                .subTitle("km/h")
                .minHeight(480)
                .minWidth(420)
                .animated(true)
                .mediumTickMarksVisible(true)
                .animationDuration(INTERVAL)
                .foregroundBaseColor(Color.WHITE)
                .build();

        lastTimerCall = System.nanoTime();
        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (now > lastTimerCall + INTERVAL * 1_000_000L) {
                    double battery = batteryGauge.getCurrentValue();
                    double speed = RND.nextDouble() * speedGauge.getRange() + speedGauge.getMinValue();
                    double currentSpeed = speedGauge.getCurrentValue();
                    double diff = Math.abs(speed - currentSpeed);
                    if (diff > MAX_THRESHOLD || diff < MIN_THRESHOLD) {
                        if (RND.nextInt() % 2 == 0 && currentSpeed + DIFF < speedGauge.getMaxValue()) {
                            speed = currentSpeed + DIFF;
                        } else if (currentSpeed - DIFF > speedGauge.getMinValue()) {
                            speed = currentSpeed - DIFF;
                        } else {
                            speed = currentSpeed;
                        }
                    }
                    if (battery < 6) {
                        batteryGauge.setValue(100);
                    } else {
                        batteryGauge.setValue(battery - 3);
                    }
                    tempGauge.setValue(RND.nextDouble() * tempGauge.getRange() / 2.5 + 20);
                    speedGauge.setValue(speed);
                    batteryGauge.setValue(battery);
                    lastTimerCall = now;
                }
            }
        };
    }

    @Override
    public void start(Stage stage) {
        HBox hBox = new HBox();
        hBox.prefHeight(480);
        hBox.prefWidth(800);
        hBox.setStyle("-fx-background-color: black;");

        VBox vBox = new VBox();
        vBox.prefWidth(400);
        vBox.prefHeight(400);
        vBox.getChildren().addAll(speedGauge);

        TextArea textArea = new TextArea();
        textArea.setPrefRowCount(2);
        textArea.setEditable(false);
        textArea.setPrefRowCount(1);
        textArea.setStyle("-fx-control-inner-background:#000000; -fx-border-width: 0; -fx-font-size: 14pt; -fx-text-fill: red;");
        textArea.setText("ERROR: wejdz na: " +
                "fb.com/PKMechPower");

        VBox vBox1 = new VBox();
        vBox1.prefWidth(300);
        vBox1.getChildren().addAll(tempGauge, batteryGauge, textArea);

        hBox.getChildren().addAll(vBox, vBox1);

        Scene scene = new Scene(hBox);

        stage.setTitle("PK racing car dashboard");
        stage.setResizable(false);
        stage.setHeight(480);
        stage.setWidth(800);
        stage.setScene(scene);
        stage.show();

        timer.start();
    }

    @Override
    public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
